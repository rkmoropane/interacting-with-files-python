import unittest
import pathlib
import json

from unittest.mock import patch, mock_open

from visitor.visitor import Visitor


class TestVisitor(unittest.TestCase):
    def setUp(self):
        self.json_files_directory = pathlib.Path("../test_json_files")

        self.alice_visitor_data = {
            "full_name": "Alice Cooper",
            "age": 25,
            "date_of_visit": "2023-06-29",
            "time_of_visit": "10:00 AM",
            "comments": "Great experience!",
            "assistant_name": "Bob",
        }

        self.alice_visitor_instance = Visitor(**self.alice_visitor_data)

    def _setup_mock_open_read_mode(self, read_data=None):
        mocked_open = mock_open(read_data=read_data)

        return mocked_open

    def _setup_mock_open_write_mode(self, write_data=None):
        mocked_open = mock_open()
        mocked_open.return_value.write.return_value = write_data

        return mocked_open

    def test_get_file_name_method_returns_correct_file_name(self):
        expected_file_name = "visitor_john_cooper.json"

        alice_file_name = Visitor._get_file_name("John Cooper")

        self.assertEqual(alice_file_name, expected_file_name)

    @patch("pathlib.Path.exists")
    def test_get_directory_path_returns_correct_directory_path(
        self, mocked_path_exists
    ):
        mocked_path_exists.return_value = True
        alice_file_name = "visitor_alice_cooper.json"

        expected_directory_path = pathlib.Path("./json_files") / alice_file_name

        file_directory_path = Visitor._get_directory_path(alice_file_name)

        mocked_path_exists.assert_called()
        self.assertEqual(file_directory_path, expected_directory_path)

    @patch.object(Visitor, "_get_directory_path")
    def test_save_method_creates_new_file_or_saves_to_existing_file(
        self, mocked_directory_path
    ):
        alice_file_name = "visitor_alice_cooper.json"

        expected_file_path = self.json_files_directory / alice_file_name
        mocked_directory_path.return_value = expected_file_path

        alice_json_data = json.dumps(self.alice_visitor_data)

        with patch(
            "builtins.open",
            self._setup_mock_open_write_mode(write_data=alice_json_data),
        ) as mocked_open_write:
            self.alice_visitor_instance.save()

        mocked_open_write.assert_called_once_with(expected_file_path, "w")
        mocked_directory_path.assert_called_once_with(alice_file_name)

    @patch("pathlib.Path.exists")
    @patch.object(Visitor, "directory_path")
    def test_load_function_reads_file_and_returns_visitor_instance(
        self, mocked_directory_path, mocked_path_exists
    ):
        mocked_path_exists.return_value = True
        mocked_directory_path.return_value = self.json_files_directory
        alice_json_data = json.dumps(self.alice_visitor_data)

        with patch(
            "builtins.open", self._setup_mock_open_read_mode(read_data=alice_json_data)
        ) as mocked_read_open:
            alice_visitor_instance = Visitor.load("Alice Cooper")

        alice_file_name = "visitor_alice_cooper.json"
        expected_file_path = Visitor._get_directory_path(alice_file_name)

        mocked_read_open.assert_called_once_with(expected_file_path, "r")
        self.assertIsInstance(alice_visitor_instance, Visitor)

    @patch("pathlib.Path.exists")
    @patch("pathlib.Path.mkdir")
    def test_load_raises_error_for_non_existing_file_or_directory(
        self, mocked_path_mkdir, mocked_path_exists
    ):
        non_existing_file_name = "visitor_charlotte_kykke.json"

        mocked_path_exists.return_value = False
        with patch.object(Visitor, "directory_path", self.json_files_directory):
            with self.assertRaises(FileNotFoundError) as context:
                Visitor.load("Charlotte Kykke")

        mocked_path_mkdir.assert_called_with(parents=True, exist_ok=True)
        self.assertIn(
            f"File {non_existing_file_name} does not exist", str(context.exception)
        )


if __name__ == "__main__":
    unittest.main()
