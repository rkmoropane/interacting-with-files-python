import json
import pathlib


class Visitor:
    directory_path = pathlib.Path("./json_files")

    def __init__(
        self,
        full_name,
        age,
        date_of_visit,
        time_of_visit,
        comments,
        assistant_name,
    ):
        self.full_name = full_name
        self.age = age
        self.date_of_visit = date_of_visit
        self.time_of_visit = time_of_visit
        self.comments = comments
        self.assistant_name = assistant_name

    def get_visitor_data(self):
        return {
            "full_name": self.full_name,
            "age": self.age,
            "date_of_visit": self.date_of_visit,
            "time_of_visit": self.time_of_visit,
            "comments": self.comments,
            "assistant_name": self.assistant_name,
        }

    @staticmethod
    def _get_file_name(full_name):
        visitor_name = full_name.lower().replace(" ", "_")
        return f"visitor_{visitor_name}.json"

    @classmethod
    def _get_directory_path(cls, file_name):
        if not cls.directory_path.exists():
            cls.directory_path.mkdir(parents=True, exist_ok=True)
        return cls.directory_path / file_name

    def save(self):
        file_name = Visitor._get_file_name(self.full_name)

        file_path = Visitor._get_directory_path(file_name)

        with open(file_path, "w") as json_file:
            json.dump(self.get_visitor_data(), json_file, indent=4)

    @classmethod
    def load(cls, full_name):
        if not isinstance(full_name, str):
            raise ValueError("Visitor full name must be a string of characters!")

        file_name = cls._get_file_name(full_name)

        file_path = cls._get_directory_path(file_name)

        if not file_path.exists():
            raise FileNotFoundError(f"File {file_name} does not exist")

        with open(file_path, "r") as json_file:
            visitor_data = json.load(json_file)
        return cls(**visitor_data)


if __name__ == "__main__":
    alice = Visitor(
        "Alice Cooper", 25, "2023-06-29", "10:00 AM", "Great experience!", "Bob"
    )
    alice.save()

    alice_data = Visitor.load("Alice Cooper")
    print("Alice comments before")
    print(alice_data.comments)
    print()

    bob = Visitor(
        "Bob Marley", 30, "2023-06-30", "11:30 AM", "Awesome place!", "Charlie"
    )
    bob.save()

    charlie = Visitor(
        "Charley Sheen", 35, "2023-07-01", "2:00 PM", "Had a fantastic time!", "Alice"
    )
    charlie.save()

    alice.comments = "Kinda weird, I don't think he'll fit in."
    alice.save()

    print("Alice comments after")
    alice_data = Visitor.load("Alice Cooper")
    print(alice_data.comments)
    print()

    charlie_data = Visitor.load("Charley Sheen")
    print("charlie comments before")
    print(charlie_data.comments)
    print()

    charlie.comments = "Winning!"
    charlie.save()
    charlie_data = Visitor.load("Charley Sheen")
    print("charlie comments after")
    print(charlie_data.comments)
