from setuptools import setup, find_packages

setup(
    name="visitor",
    version="1.0.0",
    decription="This is the package that contains information about visitors",
    author="Kholofelo Moropane",
    packages=find_packages(),
)
