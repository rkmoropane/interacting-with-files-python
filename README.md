# Interacting with files 

- Create a class called "Visitor" with the following instances:
    + full name
    + age
    + date of visit
    + time of visit
    + comments
    + name of the person who assisted the visitor
- Create a method called "save" that saves the visitor’s data to a JSON file. The file name should be named like this, "visitor_{their_full_name}.json".
```
alice.save()   # results in visitor_alice_cooper.json
bob.save()     # results in visitor_bob_marley.json
charlie.save() # results in visitor_charley_sheen.json
```

Create a method called "load" that takes in a name and then grabs a 'Visitor' object from the file. It should return a Visitor instance.
For example:
```
Visitor.load("Alice Cooper") 
# returns a Visitor instance

Visitor.load("Bob Marley")
# Same deal for good ol' Bob
```
Take note that the load function should be attached to the Visitor class, load is not a standalone function.

## How to run this project:
1. **First, git clone the project:**
```
git clone git@gitlab.com:rkmoropane/interacting-with-files-python.git
```

2. **Then, make sure that your virtual environment is created, running and active. If not, type this in your command prompt in VScode:**
```
python -m venv <name_of_virtual_environment>
```
Or in Linux/macOS:
```
python3.9 -m venv <name_of_virtual_environment>
```

3. **To activate it, type the following in your command prompt & run it:**
```
.\<name_of_virtual_environment>\Scripts\activate
```
Or in Linux/macOS:
```
source <name_of_virtual_environment>\bin\activate
```
Done. Now your virtual environment is up and running.**

4. **After, install the packages in the development mode. This will be installed in your virtual environment.**
```
python setup.py develop
```
Or in Linux/macOS:
```
python3.9 setup.py develop
```
This installs the package. Now the package can be imported from anywhere, so long as your virtual environment is active. 

5. **Now run the tests by typing:**
``` 
python tests/test_visitor.py
```
Done.